# -*- coding: utf-8 -*-
from endi_base.utils.ascii import (
    force_ascii,
    force_filename,
)


def test_force_ascii():
    assert force_ascii("Ã©co") == u"eco"
    assert force_ascii(5) == "5"
    assert force_ascii(u"Ã©co") == "eco"


def test_force_filename():
    assert force_filename(u"Ã©' ';^") == u"e_"
