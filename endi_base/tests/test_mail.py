# -*- coding: utf-8 -*-


def test_format_mail():
    from endi_base.mail import format_mail
    assert format_mail(u"contact@endi.coop") == u"<contact@endi.coop>"


def test_format_link():
    from endi_base.mail import format_link
    settings = {'mail.bounce_url': "endi.coop"}

    assert format_link(settings, "http://test.fr") == \
        "http://endi.coop/?url=http%3A//test.fr"
