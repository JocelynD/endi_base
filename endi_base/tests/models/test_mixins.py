from mock import Mock
import pytest


def test_duplicable_mixin_empty_undefined():
    from endi_base.models.mixins import DuplicableMixin

    class DummyDuplicable(DuplicableMixin, Mock):
        pass

    obj = DummyDuplicable()

    with pytest.raises(NotImplementedError):
        obj.duplicate()


def test_duplicable_mixin():
    from endi_base.models.mixins import DuplicableMixin

    class DummyDuplicable(DuplicableMixin, Mock):
        __duplicable_fields__ = [
            'a',
            'b',
        ]

    obj = DummyDuplicable(a=1, b=2, c=3)
    dup = obj.duplicate()
    assert dup.a == obj.a
    assert dup.b == obj.b
    assert dup.c != 3


def test_duplicable_mixin_adhoc_values():
    from endi_base.models.mixins import DuplicableMixin

    class DummyDuplicable(DuplicableMixin, Mock):
        __duplicable_fields__ = [
            'a',
            'b',
        ]

    obj = DummyDuplicable(a=1, b=2, c=3)
    dup = obj.duplicate(b=12, c=15)
    assert dup.a == obj.a
    assert dup.b == 12
    assert dup.c == 15
