# -*- coding: utf-8 -*-
import os
import datetime
import time
import pytest

from endi_base.models.types import (
    CustomFileType,
    CustomDateType,
    CustomDateType2,
    CustomInteger,
    PersistentACLMixin,
)


def test_bind_customfiletype():
    a = CustomFileType('test_', 255)
    cstruct1 = {'uid': 'test_testfile.jpg', 'filename': 'testfile.jpg'}
    assert a.process_bind_param(cstruct1, "nutt") == 'testfile.jpg'


def test_result_customfiletype():
    a = CustomFileType('test_', 255)
    cstruct1 = {'uid': 'test_testfile.jpg', 'filename': 'testfile.jpg'}
    assert a.process_result_value('testfile.jpg', 'nutt') == cstruct1


def test_bind_customdatetype():
    os.environ['TZ'] = "Europe/Paris"
    a = CustomDateType()
    date = datetime.datetime(2012, 1, 1, 1, 1)
    assert a.process_bind_param(date, "nutt") == 1325376060
    date = 1504545454
    assert a.process_bind_param(date, "nutt") == 1504545454
    t = int(time.time())

    assert a.process_bind_param("", "nutt") >= t
    assert a.process_bind_param("", "nutt") <= t + 6000


def test_result_customdatetype():
    os.environ['TZ'] = "Europe/Paris"
    a = CustomDateType()
    date = datetime.datetime(2012, 1, 1, 1, 1)
    timestamp = 1325376060
    assert a.process_result_value(timestamp, "nutt") == date


def test_bind_customdatetype2():
    a = CustomDateType2()
    date = datetime.date(2012, 11, 10)
    dbformat = 20121110
    assert a.process_bind_param(date, "Nutt") == dbformat


def test_result_customdatetype2():
    a = CustomDateType2()
    date = datetime.date(2012, 11, 10)
    dbformat = 20121110
    assert a.process_result_value(dbformat, "Nutt") == date


def test_bind():
    a = CustomInteger()
    vala = 1500
    valb = long(1500)
    assert a.process_bind_param(vala, "nutt") == valb


def test_result():
    a = CustomInteger()
    vala = long(1500)
    valb = 1500
    assert a.process_result_value(vala, "nutt") == valb


def test_persistentaclmixin():
    class ACLMixinSubclass(PersistentACLMixin):
        pass

    obj = ACLMixinSubclass()

    # AttributeError, if nothing is defined
    with pytest.raises(AttributeError):
        obj.__acl__

    # Use __default_acl__, if available
    ACLMixinSubclass.__default_acl__ = [
        ('Allow', 'principal', ('add', 'show'))
    ]

    assert obj.__acl__ == [
        ('Allow', 'principal', ('add', 'show'))
    ]

    # Override __default_acl__
    obj.__acl__ = [
        ('Deny', 'principal', ('add', 'show'))
    ]

    assert obj.__acl__ == [
        ('Deny', 'principal', ('add', 'show'))
    ]

    # delete __acl__, falling back to __default_acl__
    del obj.__acl__

    assert obj.__acl__ == [
        ('Allow', 'principal', ('add', 'show'))
    ]
