# -*- coding: utf-8 -*-
import datetime

from endi_base.models.utils import format_to_taskdate
from endi_base.models.utils import format_from_taskdate


def test_format_to_taskdate():
    date1 = datetime.date(2012, 1, 10)
    date2 = datetime.date(2012, 12, 10)
    date3 = datetime.date(2012, 7, 1)
    # bug #529
    date4 = datetime.date(2012, 7, 31)
    assert format_to_taskdate(date1) == 20120110
    assert format_to_taskdate(date2) == 20121210
    assert format_to_taskdate(date3) == 20120701
    assert format_to_taskdate(date4) == 20120731


def test_format_from_taskdate():
    date1 = datetime.date(2012, 1, 10)
    date2 = datetime.date(2012, 12, 10)
    date3 = datetime.date(2012, 7, 1)
    # bug #529
    date4 = datetime.date(2012, 7, 31)
    assert format_from_taskdate(20120110) == date1
    assert format_from_taskdate(20121210) == date2
    assert format_from_taskdate(20120701) == date3
    assert format_from_taskdate(20120731) == date4
