# -*- coding: utf-8 -*-


SEX_OPTIONS = (
    ('', '', ),
    ('M', 'Homme', ),
    ('F', 'Femme', ),
)


CIVILITE_OPTIONS = (
    ('', u'Non renseigné', ),
    ('Monsieur', u'Monsieur',),
    ('Madame', u'Madame',),
)
