enDI base software
=======================

Since version 6 only Python 3 is supported

Provide :

* common models
* common tools

This package provides base tools to allow splitting the software enDI in several pieces.
It provides :

* SQLAlchemy configuration (scoped session, base model class)
* Custom SQLAlchemy column types
* String formatting utilities
* Date formatting utilities
* Constants
* ...

It should not be installed directly but should be installed as a dependency
